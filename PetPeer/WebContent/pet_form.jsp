<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="styles.css">
<script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<script src="script.js"></script>
<title>Add a Pet</title>
</head>
<body>
<%
   if(session.getAttribute("user")==null){
		response.sendRedirect("login.jsp");
	}
%>
<div id='cssmenu'>
<ul>
   <li><a href='PetDetailsServlet'><span>Home</span></a></li>
   <li class='active'><a href='pet_form.jsp'><span>Add Pet</span></a></li>
   <li><a href='logout.jsp'><span>Logout</span></a></li>
</ul>
</div>
<br><br><br>
<h1 style="text-align: center;">Add Pet Information</h1>
<br><br><br><br><br>
<form  name="petForm" action="MainsServlet" method="post" style="text-align: center;">
<input type="hidden" name="jspPage" value="pet_form.jsp" />
<br>
Name: <input type="text" name="name" required><br><br>
Age:<input type="number" name="age" required><br><br>
Place:<input type="text" name="place" required><br><br>
     <input type="submit" name="save" value="Save">   <button type="reset" name="reset" value="Reset">Cancel</button></pre> 
</form>

</body>
</html>