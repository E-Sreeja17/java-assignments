package com.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.model.Pet;
import com.model.User;

public class RegistrationDao implements DaoInterface {
	
	public boolean create(User user) {
		PreparedStatement preparedStatement = null;
		String query="";
		Connection connection=DBConnection.getConnection();
		try {
			query="INSERT INTO PET_USER(USER_NAME,USER_PASSWD) VALUES(?,?)";
			preparedStatement=connection.prepareStatement(query);
			preparedStatement.setString(1, user.getUserName());
			preparedStatement.setString(2, user.getPassword());
			if(preparedStatement.execute()){
				return true;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
 
	
	public boolean read(User user) {
		PreparedStatement preparedStatement = null;
		String query="";
		ResultSet resultSet=null;
		Connection connection=DBConnection.getConnection();
		try {
			query="SELECT USER_NAME FROM PET_USER WHERE USER_NAME=?";
			preparedStatement=connection.prepareStatement(query);
			preparedStatement.setString(1, user.getUserName());
			resultSet=preparedStatement.executeQuery();
			if(!resultSet.next()){
				return false;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return true;
	}

	
	public boolean update(User user, int petId) {
		return false;
	}

	
	public boolean delete(String userId) {
		return false;
	}

	
	public String readLogin(User user) {
		return null;
	}

	
	public boolean savePet(Pet pet) {
		
		return false;
	}

	
	public List<Pet> readPet() {
		
		return null;
	}

	
	public List<Pet> readPet(User user) {
		
		return null;
	}
}