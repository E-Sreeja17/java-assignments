package assignment6;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import assignment6.UserMainCode10;

public class CompareDates {

	public static void main(String[] args) {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the date: ");
		String string1 = null;
		try {
			string1 = br.readLine();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		System.out.println("Enter the date: ");
		String string2 = null;
		try {
			string2 = br.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			System.out.println(UserMainCode10.findOldDate(string1, string2));
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
