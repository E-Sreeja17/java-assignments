/*Write a java program to print current date and time in the specified format. */


package assignment6;
import java.time.format.DateTimeFormatter;  
import java.time.LocalDateTime;    
public class DateAndTimeFormat {

	public static void main(String[] args) {
		
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
		   LocalDateTime now = LocalDateTime.now();  
		   System.out.println(dtf.format(now));  
	}

}
