package assignment6;
import java.util.Scanner;
import java.util.*;
public class NumberValidation {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		String pan=sc.next();
		int b=NumberValidation(pan);
		if(b==1)
		{
			System.out.println(" valid number format");
		}
		else
		{
			System.out.println("Invalid number format");
		}
	}

	private static int NumberValidation(String input) {
		int b=0;
		if(input.matches("[0-9]{3}[-]{1}[0-9]{3}[-]{1}[0-9]{4}"))
		{
			b=1;
		}
		else
		{
			b=0;
		}
		return b;
	}

}
