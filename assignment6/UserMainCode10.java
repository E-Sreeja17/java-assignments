package assignment6;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UserMainCode10 {
	public static String findOldDate(String string1, String string2) throws ParseException {

		SimpleDateFormat sdformat = new SimpleDateFormat("dd-MM-yyyy");
		SimpleDateFormat sdformat1 = new SimpleDateFormat("MM/dd/yyyy");
		Date d1 = sdformat.parse(string1);
		Date d2 = sdformat.parse(string2);
		if (d1.compareTo(d2) < 0) {
			String str1 = sdformat1.format(d1);
			return str1;
		} else {
			String str2 = sdformat1.format(d2);
			return str2;
		}
	}
}