/*.Write a Java program to extract date, time from the date string */


package assignment6;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class ExtractDateTime {

	public static void main(String[] args) {
		String dt = new String();
        Scanner sc =  new Scanner(System.in);
        System.out.println("Enter a String containing a date in yyyy-mm-dd format only");
        dt=sc.nextLine();
           
      try 
      {    
    //Format for string is specified here and passed as a parameter in the constructor
      Date sdf = new SimpleDateFormat("yyyy-MM-dd").parse(dt);
   //Two methods viz parse and format are used to extract Date from the string
      String date = new SimpleDateFormat("dd/MM/yyyy").format(sdf);
       System.out.println("The date is "+date+" (dd/mm/yyyy) ");
      } 
    catch (Exception e) 
      {
        System.out.println("Exception occured "+e);
     }
	}

}
