package carDealershipInventoryProject;
import java.sql.*;
import java.util.Scanner;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
public class CarInventory {
	Connection con=null; 
	Statement st=null;
	PreparedStatement ps=null;
	Scanner sc=new Scanner(System.in);
	String car_make="";
	String car_model="";
	int year;
	float price;
	public void insertMethod() throws Exception{
		System.out.println("Make: ");
		car_make=sc.next();
		System.out.println("Model: ");
		car_model=sc.next();
		System.out.println("Year: ");
		year=sc.nextInt();
		System.out.println("Price ($):");
		price=sc.nextFloat();
		try {
			con=DBConnection.getConnection(); 
		    ps=con.prepareStatement("insert into car_details(car_make, car_model, year, price) values(?,?,?,?)");
			ps.setString(1, car_make); 
			ps.setString(2, car_model);
			ps.setInt(3, year); 
			ps.setFloat(4, price);
			int noofRows=ps.executeUpdate();
			System.out.println(noofRows + " Records inserted"); 
		} catch (Exception e) { 
			System.out.println(e);

		}finally {
			ps.close();
			con.close();
		}
	}

 	public void selectMethod () throws Exception {
	try {
		con=DBConnection.getConnection();
		ps=con.prepareStatement("select * from car_details");
		ResultSet rs=ps.executeQuery();
		if(!rs.next()) {
			System.out.println("There are currently no cars in the car_details");
		}else {
			do {
				System.out.printf(rs.getInt("car_id")+" "+rs.getString("car_make")+" "+rs.getString("car_model")+ " " +rs.getInt("year")+" $"+ "%.2f\n",rs.getFloat("price")); 
			}while(rs.next());
			ps=con.prepareStatement("select count(car_id) from car_details"); 
			ResultSet noOfCars=ps.executeQuery(); 
			noOfCars.next();
			System.out.println("Number of cars: " + noOfCars.getInt("count(car_id)"));
			ps=con.prepareStatement("select sum(price) from car_details");
			ResultSet total=ps.executeQuery(); 
			total.next();
			System.out.printf("Total Inventory: $" + "%.2f\n",total.getFloat("sum(price)"));
		}
	} catch (Exception e) { 
		System.out.println(e);
	}
	
	finally {
		ps.close();
		con.close();
	}
	
	
	} 
	public void deleteMethod() throws Exception { 
		System.out.println("Enter the Id: ");
		int id=sc.nextInt();
		try {
			con= DBConnection.getConnection();
			 ps=con.prepareStatement ("delete from car_details where car_id=" + id);
			int row=ps.executeUpdate();		
			System.out.println(row+" Row deleted successfully!...");
		} catch (Exception e) {
			System.out.println(e);
		}
		
finally {
			
			ps.close();
			con.close();
		}
	}
	public void updateMethod () throws Exception {
		System.out.println("Enter the column name and value to be updated: "); 
		String column_name=sc.next();
		String value=sc.next(); 
		System.out.println("Enter the row to be updated: ");
		int id=sc.nextInt();
		try {
			con=DBConnection.getConnection();
			PreparedStatement ps=con.prepareStatement ("update car_details set " + column_name + "= ? where car_id= ?");
			ps.setString(1, value);
			ps.setInt(2, id);
			ps.executeUpdate();
			int row=ps.executeUpdate();		
			System.out.println(row+" Row updated successfully!...");
		} catch (Exception e) {
			System.out.println(e); 
		} 
finally {
			
			ps.close();
			con.close();
		}
	}
}
