/* Write a program to create a class DemoThread1 implementing Runnable interface. 
 * In the constructor, create a new thread and start the thread. In run() display a message "running child Thread in loop : " display the value of the counter ranging from 1 to 10. Within the loop put the thread to sleep for 2 seconds. 
 * In main create 3 objects of the DemoTread1 and execute the program. */



package assignment7;

class DemoThread2 extends Thread
{	DemoThread2()
	{
		Thread t1=new Thread();
		t1.start();
	}
	public void run() 
	{		
		System.out.println("running child Thread in loop : ");
		try {				
            for(int i=1; i<=10; i++) { 
            	System.out.println(i);
                Thread.sleep(2000);
            }   }  catch(InterruptedException e) { }
        }	}
public class MainExtendsDemo {
public static void main(String[] args) {		
		DemoThread2 dt=new DemoThread2();
		dt.start();
		DemoThread2 dt1=new DemoThread2();
		dt1.start();
		DemoThread2 dt2=new DemoThread2();
		dt2.start();
	}
}


