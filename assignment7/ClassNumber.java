/*Write a program to create a class Number  which implements Runnable. 
Run method displays the multiples of a number accepted as a parameter. 
In main create three objects - first object should display the multiples of 2, 
second should display the multiples of 5 and third should display the multiples of 8. 
Display appropriate message at the beginning and ending of thread. 
The main thread should wait for the first object to complete.
 Display the status of threads before the multiples are displayed and after completing the multiples.
 */

package assignment7;

class NumberDemo implements Runnable
{
   int n ;
  public NumberDemo(int n) 
   {
	   this.n=n;
   }
  public void run()
  {
     for(int i=1;i<=10;i++)
     {
    	 System.out.println(n*i);
     }
	
  }
 
}
public class ClassNumber
{
    public static void main(String args[]) 
    {
      NumberDemo t1=new NumberDemo(2);
      NumberDemo t2=new NumberDemo(5);
      NumberDemo t3=new NumberDemo(8);
      Thread newt1=new Thread(t1);
      Thread newt2=new Thread(t2);
      Thread newt3=new Thread(t3);
      System.out.println("Thread 1 started");
      newt1.start();
      try {
		newt1.join();
	  } catch (InterruptedException e) {
		e.printStackTrace();
     	}
      System.out.println("Thread 1 Completed");
      
      
      System.out.println("Thread 2 started");
      newt2.start();
      try {
		newt2.join();
	  } catch (InterruptedException e) {
		e.printStackTrace();
     	}
      System.out.println("Thread 2 Completed");
      
      
      System.out.println("Thread 3 started");
      newt3.start();
      try {
		newt3.join();
	  } catch (InterruptedException e) {
		e.printStackTrace();
     	}
      System.out.println("Thread 3 Completed");
      
    }
}
