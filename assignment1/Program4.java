package assignment1;
import java.util.Scanner;
public class Program4 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		   int a, b;
		   a = sc.nextInt();
		   b = sc.nextInt();
		   System.out.println("Before swapping : a, b = "+a+", "+ b);
		   a = a + b;
		   b = a - b;
		   a = a - b;
		   System.out.println("After swapping : a, b = "+a+", "+ b);
	}

}
