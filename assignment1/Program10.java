package assignment1;
import java.util.Scanner;

class UserMainCode1{
	public static String getLongestWord(String sent){
		String[] strarr=sent.split(" ");
		String maxstr=strarr[0];
		for (int i=0;i<strarr.length;i++){
			if (strarr[i].length()>maxstr.length()){
				maxstr=strarr[i];
			}
		}
		return maxstr;
	}
}
public class Program10 {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		String str=sc.nextLine();
		System.out.println(UserMainCode1.getLongestWord(str));
		sc.close();
	}

}
