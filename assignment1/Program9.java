package assignment1;
import java.util.Scanner;
class UserMainCode{
	public static int sumOfSquaresOfEvenDigits(int num){
		int rem;
		int sqsum=0;
		if (num>0){
			while (num>0){
				rem=num%10;
				num=num/10;
				if (rem%2==0){
					sqsum=sqsum+(rem*rem);
				}
			}
			if(sqsum==0){
				return 1;
			}
			else{
				return sqsum;
			}
		}
		else {
			return 0;
		}
	}
}
public class Program9 {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter a positive number");
		int n=sc.nextInt();
		int res=UserMainCode.sumOfSquaresOfEvenDigits(n);
		if (res==0){
			System.out.println("Enter a positive number");
		}
		else if(res==1){
			System.out.println("No even digits. 0");
		}
		else{
			System.out.println("Sum of square of the even digits is "+res);
			sc.close();
		}

	}

}
