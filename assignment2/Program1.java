package assignment2;
import java.util.Scanner;
class Small{
	public static int smallestOfThree(int num1,int num2,int num3){
		int[] arr=new int[]{num1,num2,num3};
		int min=arr[0];
		for (int i=0;i<arr.length;i++){
			if (arr[i]<min){
				min=arr[i];
			}
		}
		return min;
	}
}
public class Program1 {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter three numbers seperated by one space");
		int n1=sc.nextInt();
		int n2=sc.nextInt();
		int n3=sc.nextInt();
		System.out.println("The smallest among the three is "+Small.smallestOfThree(n1,n2,n3));
		sc.close();

}
}
