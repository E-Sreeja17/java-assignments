package assignment2;
class a{
	public void display(){
		System.out.println("A from a");
	}
}
class b extends a{
	public void display(){
		System.out.println("B from b");
	}
}
public class Program5 {

	public static void main(String[] args) {
		a obj1=new a();
		b obj2=new b();
		a obj12=new b();
		obj1.display();
		obj2.display();
		obj12.display();
	}

}
