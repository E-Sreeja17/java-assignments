package assignment2;
public class Room {
public int roomNo;
public String roomType;
public double roomArea;
public String ACMachine;

public void setData(int rn,String rt,double ra,String am){
	roomNo=rn;
	roomType=rt;
	roomArea=ra;
	ACMachine=am;
}
public void displayData(){
	System.out.println("Room Number: "+roomNo);
	System.out.println("Room Type: "+roomType);
	System.out.println("Room Area: "+roomArea);
	System.out.println("AC Machine: "+ACMachine);
}
public class Program4 {

	public void main(String[] args) {
		Room room1=new Room();
		room1.setData(1, "AC", 125, "LG");
		room1.displayData();
	}
	}

}
