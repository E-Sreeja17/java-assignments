package assignment2;
import java.util.Scanner;

class Count{
	public static int countVowelsInString(String s){
		int counter=0;
		for (int i=0;i<s.length();i++){
			String vow=Character.toString(s.charAt(i));
			if (vow.equalsIgnoreCase("a") || vow.equalsIgnoreCase("e") || vow.equalsIgnoreCase("i") || vow.equalsIgnoreCase("o") || vow.equalsIgnoreCase("u")){
				counter++;
			}
		}
		return counter;
	}
}
public class Program3 {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		String str=sc.nextLine();
		System.out.println("The number of vowels in this word is "+Count.countVowelsInString(str));
		sc.close();
	}

}
