package assignment4;
import java.util.Scanner;
public class Program3 {

	public static void main(String[] args) {
		System.out.println("Select the card\n1.Membership Card\n2.Payback Card");
		Scanner sc=new Scanner(System.in);
		int choice=sc.nextInt();
		if (choice==1){
			sc.nextLine();
			System.out.println("Enter the card details seperated by a |:");
			String info=sc.nextLine();
			System.out.println("Enter the rating");
			Integer rating=Integer.valueOf(sc.nextInt());
			String[] strarr=info.split("\\|");
			MembershipCard mc=new MembershipCard(strarr[0],strarr[1],strarr[2],rating);
			System.out.println(mc.getHolderName()+"'s Membership Card Details");
			System.out.println("Card Number "+mc.getCardNumber());
			System.out.println("Expiry Date "+mc.getExpiryDate());
			System.out.println("Rating "+mc.getRating());
			
		}
		else if(choice==2){
			sc.nextLine();
			System.out.println("Enter the card details seperated by a |:");
			String info=sc.nextLine();
			String[] strarr=info.split("\\|");
			System.out.println("Enter points in card");
			Integer points=Integer.valueOf(sc.nextInt());
			System.out.println("Enter the total amount");
			Double totalAmount=Double.valueOf(sc.nextDouble());
	        PaybackCard pc=new PaybackCard(strarr[0], strarr[1], strarr[2], points, totalAmount);
	        System.out.println(pc.getHolderName()+"'s Payback Card Details:\n"+"Card Number "+pc.getCardNumber());
	        System.out.println("Expiry Date "+pc.getExpiryDate());
	        System.out.println("Points Earned "+pc.getPointsEarned());
	        System.out.println("Total Amount "+pc.getTotalAmount());
		}
		sc.close();
	}

}
