package assignment4;
import java.util.Scanner;

class MyCalculator implements AdvancedArithmetic {
	public int divisor_sum(int n){
		int sum=0;
		for (int i=1;i<=n;i++){
			if (n%i==0){
				sum=sum+i;
			}
		}
		return sum;
	}
public class MyCalculator {

	public static void main(String[] args) {
		MyCalculator mc=new MyCalculator();
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number");
		int n=sc.nextInt();
		System.out.println(mc.divisor_sum(n));
		sc.close();
	}

}
