package assignment5;
import java.io.*;
public class CheckedException {

	public static void main(String[] args) {
		 String filename = "C:\\CheckedExceptions.txt";
		    File file = new File(filename);
		    try {
				FileReader fileReader = new FileReader(file);
			} 
		    catch (FileNotFoundException e) {				
				System.out.println(e);
			}
	}

}
