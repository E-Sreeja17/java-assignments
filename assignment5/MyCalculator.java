package assignment5;


import java.util.*;
import java.util.Scanner;

class Solution{
    public long power(int n, int p) throws Exception{
    	if(n == 0 && p == 0)
            throw new Exception("n and p should not be zero.");
        else if(n < 0 || p < 0)
            throw new Exception("n or p should not be negative.");
        else
            return (long)(Math.pow(n,p));
    }
}
public class MyCalculator {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

        while(sc.hasNextInt())
        {
            int n = sc.nextInt();
            int p = sc.nextInt();
            Solution M = new Solution();
            try
            {
                System.out.println(M.power(n,p));
            }
            catch(Exception e)
            {
                System.out.println(e);
            }
        }
	}

}
