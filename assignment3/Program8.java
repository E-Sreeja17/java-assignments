package assignment3;
import java.util.Scanner;
public class UserMainCode3 {
	public static String getString(String old){
		if (old.charAt(0)=='k' || old.charAt(0)=='K'){
			String mod=old.charAt(0)+old.substring(2);
			return mod;
		}
		else if(old.charAt(1)=='b' || old.charAt(1)=='B'){
			String mod=old.charAt(1)+old.substring(2);
			return mod;
		}
		else{
			String mod=old.substring(2);
			return mod;
		}
	}

}
public class Program8 {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the string");
		String str=sc.nextLine();
		System.out.println(UserMainCode3.getString(str));
		sc.close();
	}

}
