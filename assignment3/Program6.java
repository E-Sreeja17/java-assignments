package assignment3;
import java.util.Scanner;
class Palind{
	public static boolean isPalindrome(String sent){
		boolean status;
		StringBuilder sb=new StringBuilder(sent);
		String str=sb.reverse().toString();
		if (sent.equals(str)){
			status=true;
			return status;
		}
		else{
			status=false;
			return status;
		}
	}
}
public class Program6 {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		String str=sc.nextLine();
		if (Palind.isPalindrome(str)){
			System.out.println("Yes, it is palindrome");
		}
		else
		{
			System.out.println("No, it is not a palindrome");
		}
		sc.close();
	}

}
