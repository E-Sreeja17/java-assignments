package assignment3;
import java.util.Scanner;
public class Program5 {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the string");
		String str=sc.nextLine();
		System.out.println("Enter the start and end indices");
		int a=sc.nextInt();
		int b=sc.nextInt();
		System.out.println(str.substring(a,b));
		sc.close();
	}

}
